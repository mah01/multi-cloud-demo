resource "digitalocean_app" "this" {
  spec {
    name   = "multi-cloud-demo"
    region = "ams"

    service {
      name               = "multi-cloud-demo"
      instance_count     = 1
      instance_size_slug = "basic-xxs"
      http_port          = 8080

      image {
        registry_type = "DOCKER_HUB"
        registry      = "maksimsorokin"
        repository    = "app-with-vault"
        tag           = "v2"
      }

      env {
        key   = "PROVIDER_NAME"
        value = "Digital Ocean"
        type  = "GENERAL"
        scope = "RUN_TIME"
      }

      env {
        key   = "VAULT_HOST"
        value = "54.216.202.53"
        type  = "GENERAL"
        scope = "RUN_TIME"
      }

      env {
        key   = "VAULT_PORT"
        value = "8200"
        type  = "GENERAL"
        scope = "RUN_TIME"
      }

      env {
        key   = "VAULT_TOKEN"
        value = "s.LBRZood9fpTcIzpLaKk4cbap"
        type  = "GENERAL"
        scope = "RUN_TIME"
      }

      routes {
        path = "/"
      }
    }
  }
}
