resource "azurerm_resource_group" "this" {
  name     = "multi-cloud-demo"
  location = "West Europe"
}

module "web_app_container" {
  depends_on = [
    azurerm_resource_group.this
  ]

  source              = "innovationnorway/web-app-container/azurerm"
  name                = "dk-sorokin-multi-cloud-demo"
  resource_group_name = azurerm_resource_group.this.name
  container_type      = "docker"
  container_image     = "maksimsorokin/app-with-vault:v2"
  port                = 8080

  app_settings = {
    PROVIDER_NAME = "Azure"
    VAULT_HOST    = "54.216.202.53"
    VAULT_PORT    = "8200"
    VAULT_TOKEN   = "s.LBRZood9fpTcIzpLaKk4cbap"
  }
}
