package dk.sorokin.multicloud;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    private final static String ENV_PROVIDER_NAME = "PROVIDER_NAME";

    @GetMapping("/")
    public String hello() {
        return String.format("Hello from [%s]", System.getenv(ENV_PROVIDER_NAME));
    }
}
