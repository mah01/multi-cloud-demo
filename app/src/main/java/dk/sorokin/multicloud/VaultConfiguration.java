package dk.sorokin.multicloud;

import org.springframework.context.annotation.Configuration;
import org.springframework.vault.authentication.ClientAuthentication;
import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.config.AbstractVaultConfiguration;

@Configuration
public class VaultConfiguration extends AbstractVaultConfiguration {

    @Override
    public VaultEndpoint vaultEndpoint() {
        String host = System.getenv("VAULT_HOST");
        int port = Integer.parseInt(System.getenv("VAULT_PORT"));

        VaultEndpoint vaultEndpoint = VaultEndpoint.create(host, port);
        vaultEndpoint.setScheme("http");

        return vaultEndpoint;
    }

    @Override
    public ClientAuthentication clientAuthentication() {
        String token = System.getenv("VAULT_TOKEN");

        return new TokenAuthentication(token);
    }
}
