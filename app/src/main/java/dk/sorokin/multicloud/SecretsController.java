package dk.sorokin.multicloud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecretsController {

    @Autowired
    private VaultTemplate vaultTemplate;

    @GetMapping("/secret")
    public String secret() {
        VaultResponse vaultResponse = vaultTemplate.read("secret/secrettwo");
        return String.format("secret/secrettwo value is [%s]", vaultResponse.getData().get("testhest"));
    }
}
